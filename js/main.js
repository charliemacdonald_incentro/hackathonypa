var GiphyApi = "http://api.giphy.com/v1/gifs/search?";
var GiphyApiKey = "&api_key=ERDU9GD981bySTTTbWr3b5YJn6IsiNla";


function uploadFile() {
    event.preventDefault(); // Totally stop stuff happening

    //Grab the file and asynchronously convert to base64.
    var file = $('#fileform [name=fileField]')[0].files[0];
    console.log(file);
    var reader = new FileReader()
    reader.onloadend = processFile
    reader.readAsDataURL(file);
}

function processFile(event) {
    var content = event.target.result;
    sendFiletoCloudVision(content.replace('data:image/jpeg;base64,', ''));
}

function sendFiletoCloudVision(content) {

    var request = {
        requests: [{
            image: {
            content: content
            },
            features: [{
            type: "LABEL_DETECTION",
            maxResults: 5
            }]
        }]
    };

    console.log(request)

    $('#results').text('Loading...');
    $.post({
      url: "https://vision.googleapis.com/v1/images:annotate?key=AIzaSyA0uGBhheTlElzj4wLcMHelVTkg0oCemgo",
      data: JSON.stringify(request),
      contentType: 'application/json'
    }).done(getGif)
    .fail(function (jqXHR, textStatus, errorThrown) {
        $('#results').text('ERRORS: ' + textStatus + ' ' + errorThrown);
    });

}

function getGif (data) {

    console.log(data);
    //var keyword = data.responses[0].labelAnnotations[0].description;
    var keywords = [];
    var inbetween = "";
    var length = data.responses[0].labelAnnotations.length;
    for (i = 0; i < length; i++) {
        console.log(data.responses[0].labelAnnotations[i].description);
        inbetween += data.responses[0].labelAnnotations[i].description + " ";
    }
    keywords = inbetween.split(" ");
    keyword = keywords.join("+");

    console.log(keyword);
    var query = "&q=" + keyword;
    var limit = "&limit=1";
    var url = (GiphyApi + GiphyApiKey + query + limit);
    var result = $.get(url);

    result.done(function(data) {
        getResult(data)
    });
    result.fail(function(data) {
        console.log(data);
    });
}

function getResult (data) {
    var gifUrl = data.data[0].images.original.url;
    console.log(gifUrl);

    var image_gif = document.createElement("img");
    var imageParent = document.getElementsByClassName("gifwrapper")[0];
    image_gif.className = "image_gif";
    image_gif.src = gifUrl;           // image.src = "IMAGE URL/PATH"
    image_gif.height = 250;
    image_gif.width = 350;
    imageParent.appendChild(image_gif);

    return gifUrl;
}

var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
        var output = document.getElementById('output');
        output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
};


